import sys
import numpy as np
for dSNR in np.arange(1.5,5):
    for fSNR in np.arange(1,dSNR,0.5):
        sys.argv = ["code/casa_photometry.py", "-c", "snrs", "-o", "G005.9+03.1", "-i", "photometry/", "-s", "SNR_cutouts/",
            "-x", "-p", "s{}_{}_".format(dSNR,fSNR), "-y", str(dSNR), "-j", str(dSNR), "-g", str(fSNR), "-b072-080",
                    "-q1.5","-a2"]
        execfile("code/casa_photometry.py")