import numpy as np
from casatasks import immath, exportfits
import ibplotting.plotlibs as ibplot
import os
from pprint import pprint

class CasaPhotImages:
    """"""

    def __init__(self, photresults ):
        """Constructor for CasaPhotImages"""
        self.results = photresults

    @property
    def results(self):
        return self.__results

    @results.setter
    def results(self, results):
        self.__results = results

# TODO
    def make_fit_images(self):

        if not os.path.exists(self.results['fitsfile']):
            return False


        cont_levels = np.array([2., 3., 5., 8., 12., 17., 23.]) * (float(self.results['sigma'])) + \
                      self.results['rms']

        rms_levels = np.array([1.]) * float(self.results['rms'])

        fit_crosses = [[self.results['fit_posra'], self.results['fit_posdec'], self.results['name']]]

        immath([self.results['imfile'], self.results['modelfile']],
               'evalexpr', self.results['residualimage'], expr='IM0-IM1')

        exportfits(self.results['residualimage'], self.results['residualfits'])

        diam = None
        if self.results['majdiam'] is not None:
            diam = self.results['majdiam'] / 3600.

        ibplot.photFigure(self.results['residualfits'],
                          self.results['residualpngfile'],
                          self.results['draj2000'],
                          self.results['ddecj2000'],
                          inR_ann=self.results['annulus_in'] / 3600.,
                          inR_dann=self.results['annulus_out'] / 3600.,
                          inR_app=self.results['aperture'] / 3600.,
                          PNDiam=diam,
                          cLevels=cont_levels,
                          # -1,
                          rLevels=rms_levels,
                          crosses=fit_crosses,
                          imsize=2. * self.results['annulus_out'] / 3600.)

        exportfits(self.results['modelfile'], self.results['modelfits'])

        ibplot.photFigure(self.results['fitsfile'],
                          self.results['pngfile'],
                          self.results['draj2000'],
                          self.results['ddecj2000'],
                          inR_ann=self.results['annulus_in'] / 3600.,
                          inR_dann=self.results['annulus_out'] / 3600.,
                          inR_app=self.results['aperture'] / 3600.,
                          PNDiam=diam,
                          cLevels=cont_levels,
                          # -1,
                          rLevels=rms_levels,
                          crosses=fit_crosses,
                          imsize=2. * self.results['annulus_out'] / 3600.)

        return True

    def make_blob_images(self):

        if not os.path.exists(self.results['maskedblobsfits']):
            return False

        cont_levels = np.array([1.]) * self.results['dSNR'] * self.results['rms']
        rms_levels = np.array([1.]) * self.results['fSNR'] * self.results['rms']

        diam = None
        if self.results['majdiam'] is not None:
            diam = self.results['majdiam'] / 3600.

        ibplot.photFigure(self.results['maskedblobsfits'],
                          self.results['maskedblobspng'],
                          self.results['draj2000'],
                          self.results['ddecj2000'],
                          inR_ann=self.results['annulus_in'] / 3600.,
                          inR_dann=self.results['annulus_out'] / 3600.,
                          inR_app=self.results['aperture'] / 3600.,
                          PNDiam=diam,
                          cLevels=cont_levels,
                          # -1,
                          valmin=1E-6,
                          valmax=self.results['dSNR'] * self.results['rms'] * 2,
                          rLevels=rms_levels,
                          imsize=1.2 * self.results['annulus_out'] / 3600.)
