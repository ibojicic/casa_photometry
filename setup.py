from setuptools import setup

setup(
    name='mwa_photometry',
    version='0.1',
    py_modules=['mwa_photometry'],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
        'aegean_run=aegean_run:cli',
        'aegean_images=aegean_images:cli'

        ]},
)