import sys

from CasaPhotImagesClass import CasaPhotImages

from ibphotometry.SetObservationClass import SetObservation
from ibphotometry.CasaBlobPhotometryClass import CasaBlobPhotometry
from ibphotometry.PhotInOutClass import PhotInOut

from pprint import pprint

if __name__ == "__main__":

    phot_inout = PhotInOut()



    for curr_object in phot_inout.targets_dict:

        #

        obs = SetObservation(curr_object, phot_inout.args_dict())

        photo = CasaBlobPhotometry(obs)
        print("Working on id={}".format(curr_object['name']))

        # try:

        if obs.args['gaussflag']:
            photo.gaussfit()

        if obs.args['blobflag']:
            photo.blobit()

        photres = photo.out_results()

        pprint(photres)

        images = CasaPhotImages(photres)

        # images.make_fit_images()
        images.make_blob_images()

        # pprint(photres)

        phot_inout.write_to_photometry(photres)
        print("finished observations")



