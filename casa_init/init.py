#!/usr/bin/env python
print 'preppending my script directory to the PYTHONPATH.'
import sys
sys.path.append("/Users/ibojicic/anaconda3/envs/ibconda2/lib/python2.7/site-packages")
sys.path.append("/Users/ibojicic/data/Code/PycharmProjects/iblibs")
sys.path.append("/Users/ibojicic/data/Code/PycharmProjects/casa_photometry")
sys.path.append("/Users/ibojicic/data/Code/PycharmProjects/peeweedbmodels")

#sys.path.append("/usr/local/Montage")

#import site
#site.addsitedir("/Users/ibojicic/.casa/lib/python2.7/site-packages")
#site.addsitedir("/Users/ibojicic/.casa/lib/python/site-packages")