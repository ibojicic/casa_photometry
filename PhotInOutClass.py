import argparse
from peeweedbmodels import dbMainGPN, dbMainMisc, dbMainSNR, dbPhotometry

from datetime import datetime
from pprint import pprint


class PhotInOut:

    def __init__(self):
        self.__table = None
        self.__phototable = dbPhotometry.Photometry
        self.__argstable = dbPhotometry.Arguments
        self.__obstable = dbPhotometry.Observations

        self.__args = None
        self.__targets = None
        self.__targets_dict = None

        self.set_args()
        self.set_table()
        self.set_objects()

    @property
    def input_table(self):
        return self.__table

    def set_table(self):
        tablemap = {'pne': dbMainGPN.PnMain, 'snrs': dbMainSNR.GalacticSnRs, 'misc': dbMainMisc.MiscObjects}
        self.__table = tablemap[self.args.catalogue]
        return self

    @property
    def phototable(self):
        return self.__phototable

    @property
    def argstable(self):
        return self.__argstable

    @property
    def obstable(self):
        return self.__obstable

    @property
    def args(self):
        return self.__args

    @property
    def objects(self):
        return self.__targets

    def set_args(self):
        # parse inputs
        parser = argparse.ArgumentParser(description='Parse input for the photometry.')

        # object selection
        parser.add_argument('--catalogue', '-c', action='store', choices=['pne', 'snrs', 'misc'], required=True,
                            type=str, help="Choose between prepared catalogues.")

        parser.add_argument('--target', '-o', action='store', required=False, default='all', type=str,
                            help="Choose object by identification (all for all objects from the chosen cat).")

        # path selection
        parser.add_argument('--resultspath', '-i', action='store', required=True, type=str,
                            help="Full path to the folder where results will be stored.")

        parser.add_argument('--infile', '-f', action='store', required=True, default='default', type=str,
                            help="Full path to the input image.")

        parser.add_argument('--prefix', '-p', action='store', required=False, type=str, default="",
                            help="Prefix to be appended to resulting file names (default '').")

        parser.add_argument('--bckg_regions', '-e', action='store', required=False, type=str, default=None,
                            help="Regions file of point source objects to be subtracted. "
                                 "Format is: dra,ddec; one per line")


        # photometry selection
        parser.add_argument('--blobpix_minmax', '-u', action='store', required=False, type=tuple, default=(None, None),
                            help="Blobcat minpix and maxpix factors (as tuple e.g. (100,200). Default no constraints. ")

        parser.add_argument('--annulus_in', '-q', action='store', required=False, type=float, default=1.5,
                            help="Annulus in factor. Default = 1.5.")

        parser.add_argument('--annulus_out', '-n', action='store', required=False, type=float, default=4.,
                            help="Annulus out factor. Default = 4.")

        parser.add_argument('--fix_offset', '-x', action='store_true',
                            help="Fix offset for imfit. The fixed offset is estimated from the annulus. (Default no)")

        parser.add_argument('--blobflag', '-v', action='store_true',
                            help="Apply blobfit (Default no)")

        parser.add_argument('--gaussflag', '-z', action='store_true',
                            help="Apply gauss fit (Default no)")

        parser.add_argument('--dSNR', '-j', action='store', required=False, type=float, default=4.,
                            help="dSNR parameter for blobcat. Default = 4")

        parser.add_argument('--fSNR', '-g', action='store', required=False, type=float, default=4.,
                            help="fSNR parameter for blobcat. Default = 3")

        parser.add_argument('--aperture', '-a', action='store', required=False, type=float, default=None,
                            help="Aperture radius in arcsec, if it's not provided maj diameter is used.")

        # TODO
        parser.add_argument('--rms', '-m', action='store', required=False, type=float, default=None,
                            help="Fixed rms noise. Default = determined by the imstat")


        # database selection
        parser.add_argument('--nowrite_db', '-d', action='store_true',
                            help="Do not write results to the database. (Default write enabled)")

        self.__args = parser.parse_args()

    @property
    def targets(self):
        return self.__targets

    @property
    def targets_dict(self):
        return self.__targets_dict

    def set_objects(self):
        targets = self.input_table.select(self.input_table.name,
                                          self.input_table.draj2000,
                                          self.input_table.ddecj2000,
                                          self.input_table.majdiam) \
            .where(self.input_table.flag == 'y')

        if self.args.target != 'all':
            targets = targets.where(self.input_table.name == self.args.target)


        if targets.count() > 0:
            self.__targets = targets
            self.__targets_dict = [row for row in targets.dicts()]

            return True

        return False

    def write_to_photometry(self, results):
        if not self.args.nowrite_db:
            self.phototable.create(date = datetime.now(), **results)
            self.argstable.create(**results)
            self.obstable.create(**results)
        else:
            print("Writing to the db disabled.")



    def args_dict(self):
        return vars(self.args)