import os, sys

import casatasks as ct

from astropy import units as u
import csv
import math
import ibcommon.parse as ibparse



class SetObservation:

    outfiles = {
        'imfile'         : ['input', 'im'],
        'fitsfile'       : ['input', 'fits'],
        'imcleaned'      : ['cleaned', 'im'],
        'fitscleaned'    : ['cleaned', 'fits'],
        'fitsfile_blobs' : ['blobs', 'fits'],
        'imfile_blobs'   : ['blobs', 'im'],
        'pngfile'        : ['phot', 'png'],
        'modelfile'      : ['model', 'im'],
        'modelfits'      : ['model', 'fits'],
        'logtable'       : ['table', 'log'],
        'residualimage'  : ['residual', 'im'],
        'residualfits'   : ['residual', 'fits'],
        'residualpngfile': ['residual', 'png'],
        'maskedblobs'    : ['maskedblobs', 'im'],
        'maskedblobsfits': ['maskedblobs', 'fits'],
        'maskedblobspng' : ['maskedblobs', 'png'],
        'summaryfile'    : ['summary', 'txt'],
        'imfitestims'    : ['estimates', 'txt']
        }

    def __init__(self, target, inout):
        """
        Set observation and photo parameters
        @param target: dictionary
        {
            'ddecj2000': -69.8472, -> RAJ2000 in deg
            'draj2000': 73.6449, -> DEJ2000 in deg
            'majdiam': None, -> maj diameter
            'name': u'test1' -> name of the source
        }
        @param inout: dictionary
        {
            'annulus_in': 1.5, -> size factor for annulus in (x aperture size)
            'annulus_out': 4.0, -> size factor for annulus out (x aperture size)
            'aperture': None, -> aperture size in arcsec
            'bckg_regions': None, -> file with list of bckg sources to be removed
            'blobflag': False, -> flag blobcat photometry
            'blobpix_minmax': (None, None), min and max for blobcat no of pixels
            'catalogue': 'misc', -> MySQL catalogue of sources NOT NEEDED
            'dSNR': 4.0, -> dSNR parameter for blobcat
            'fSNR': 4.0, -> fSNR parameter for blobcat
            'fix_offset': False, -> fixoffset flag in CASA IMFIT
            'gaussflag': True, -> flag for gauss photometry
            'infile': 'fitstests/LMC_875MHz_2019.fits', -> input fits file
            'nowrite_db': False, -> TODO
            'prefix': '', -> TODO
            'resultspath': 'results', -> path for the result files
            'rms': None, -> TODO
            'target': '6' -> NOT NEEDED
        }
                    """
        self.target = target
        self.args = inout

        # self.bckg = []
        # self.parse_bckg_file()
        self.objectfolder = None
        self.files = {}
        self.pars = None

        self.create_object_folder()
        self.set_out_files()
        self.create_imfile()
        self.set_obs_pars()

    @property
    def pars(self):
        return self.__pars

    @pars.setter
    def pars(self, pars):
        self.__pars = pars

    @property
    def target(self):
        return self.__target

    @target.setter
    def target(self, target):
        self.__target = target

    @property
    def args(self):
        return self.__args

    @args.setter
    def args(self, args):
        self.__args = args

    @property
    def objectfolder(self):
        return self.__objectfolder

    @objectfolder.setter
    def objectfolder(self, folder):
        self.__objectfolder = folder

    def construct_file_name(self, outfile):
        return '{}{}_{}.{}'. \
            format(self.objectfolder, self.target['name'], self.outfiles[outfile][0], self.outfiles[outfile][1])

    def create_object_folder(self):
        folder = "{}{}".format(ibparse.corrpath(self.args['resultspath']), self.target['name'])
        if os.path.exists(folder):
            sys.exit("Output folder for object: '{}' already exists, exiting...".format(folder))
        os.makedirs(folder)
        if not os.path.exists(folder):
            sys.exit("Can't create output folder for object: '{}' already exists, exiting...".format(folder))
        self.objectfolder = ibparse.corrpath(folder)
        return self

    def create_imfile(self):
        if not os.path.exists(self.files['imfile']):
            ct.importfits(fitsimage=self.args['infile'], imagename=self.files['imfile'])
        return self

    # TODO
    def set_out_files(self):
        for outf in self.outfiles:
            self.files[outf] = self.construct_file_name(outf)
        return self

    # background objects for subtracting
    def parse_bckg_file(self):
        result = []
        if self.args['bckg_regions'] is not None and os.path.exists(self.args['bckg_regions']):
            with open(self.args['bckg_regions'], 'rb') as f:
                reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
                result = list(reader)
        return result

    def set_obs_pars(self):
        pars = dict()

        pars['cdelt1'] = self.radstoarcsecs(
            float(ct.imhead(imagename=self.files['imfile'], mode='get', hdkey='CDELT1')['value']))
        pars['cdelt2'] = self.radstoarcsecs(
            float(ct.imhead(imagename=self.files['imfile'], mode='get', hdkey='CDELT2')['value']))

        pars['use_bmaj'] = float(ct.imhead(imagename=self.files['imfile'], mode='get', hdkey='BMAJ')['value'])
        pars['use_bmin'] = float(ct.imhead(imagename=self.files['imfile'], mode='get', hdkey='BMIN')['value'])
        pars['use_bpa'] = float(ct.imhead(imagename=self.files['imfile'], mode='get', hdkey='BPA')['value'])

        if pars['use_bmaj'] < 1.:
            pars['use_bmaj'] = float(ct.imhead(imagename=self.files['imfile'], mode='get', hdkey='BMAJ')['value'])
            pars['use_bmin'] = float(ct.imhead(imagename=self.files['imfile'], mode='get', hdkey='BMIN')['value'])

        pars['beam_area'] = pars['use_bmaj'] * pars['use_bmin'] * math.pi / (4. * math.log(2.))

        # if aperture size is not provided use maj diameter
        # if no maj diameter use maj beam size
        pars['aperture'] = self.args['aperture']
        if pars['aperture'] is None:
            if self.target['majdiam'] is None:
                pars['aperture'] = pars['use_bmaj']
                pars['resolved'] = False
            else:
                pars['aperture'] = float(self.target['majdiam']) / 2.
                pars['resolved'] = pars['use_bmaj'] / self.target['majdiam'] < 2.

        # TODO check if annulus is within image
        pars['annulus_in'] = pars['aperture'] * self.args['annulus_in']
        pars['annulus_out'] = pars['aperture'] * self.args['annulus_out']

        # # define regions
        # if self.object.photdraj2000 is not None and self.object.photddecj2000 is not None:
        #     self.object.draj2000 = self.object.photdraj2000
        #     self.object.ddecj2000 = self.object.photddecj2000

        pars['bckg_sources'] = self.parse_bckg_file()

        # from input arguments
        pars['fix_offset'] = self.args['fix_offset']
        pars['dSNR'] = self.args['dSNR']
        pars['fSNR'] = self.args['fSNR']
        pars['blobpix_minmax'] = self.args['blobpix_minmax']
        pars['infile'] = self.args['infile']


        self.pars = pars
        return self

    @staticmethod
    def radstoarcsecs(value):
        x = value * u.rad
        arcs = x.to(u.arcsec)
        return arcs.value

