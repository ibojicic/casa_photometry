import os
import shutil
import math
import ibcommon.strings as ibstrings

import casatasks as ct

import ibcommon.dicts as ibdicts

from ibcasa.casafncs import run_imstat, write_estimates, run_imfit, subtract_bckgs, \
    parse_fit, make_annulus, make_aperture
from ibastro.coordtrans import radec_separation

from pprint import pprint


class CasaBlobPhotometry:
    __fitresults = {}
    __blobpars = {}
    __blobresults = {}
    __bckgstats = {}
    __fit_estimates_file = ''
    # current im file for proccessing
    __imfile = None
    # current fits file for proccessing
    __fitsfile = None
    # zero level for blob masking
    # flux is ALWAYS assumed to be positive
    zerolevel = 0
    # exclude level for blob maskig
    # flux bellow this level is taken into consideration
    masklevel = 999

    def __init__(self, observation):
        """
        Apply photometry
        @param observation: object
        observation.files:
        {
            'fitscleaned': 'results/test1/test1_cleaned.fits',
            'imcleaned': 'results/test1/test1_cleaned.im',
            'imfile': 'results/test1/test1_input.im',
            'imfile_blobs': 'results/test1/test1_blobs.im',
            'fitsfile_blobs': 'results/test1/test1_blobs.fits',
            'imfitestims': 'results/test1/test1_estimates.txt',
            'fitsfile': 'results/test1/test1_input.fits',
            'logtable': 'results/test1/test1_table.log',
            'maskedblobs': 'results/test1/test1_maskedblobs.im',
            'maskedblobsfits': 'results/test1/test1_maskedblobs.fits',
            'maskedblobspng': 'results/test1/test1_maskedblobs.png',
            'modelfile': 'results/test1/test1_model.im',
            'modelfits': 'results/test1/test1_model.fits',
            'pngfile': 'results/test1/test1_phot.png',
            'residualfits': 'results/test1/test1_residual.fits',
            'residualimage': 'results/test1/test1_residual.im',
            'residualpngfile': 'results/test1/test1_residual.png',
            'summaryfile': 'results/test1/test1_summary.txt'
        }

        observation.pars:
        {
            'infile': 'fitstests/LMC_875MHz_2019.fits', -> input fits file
            'annulus_in': 20.8,
            'annulus_out': 55.4,
            'aperture': 13.8,
            'bckg_sources': [],
            'beam_area': 190.2,
            'cdelt1': -2.00000000000016,
            'cdelt2': 2.00000000000016,
            'resolved': False,
            'use_bmaj': 13.8,
            'use_bmin': 12.1,
            'use_bpa': -84.4,
            'fix_offset': False, -> fixoffset flag in CASA IMFIT
            'dSNR': 4.0, -> dSNR parameter for blobcat
            'fSNR': 4.0, -> fSNR parameter for blobcat
            'blobpix_minmax': (None, None), min and max for blobcat no of pixels
        }

        observation.target:
        {
            'ddecj2000': -69.8472, -> RAJ2000 in deg
            'draj2000': 73.6449, -> DEJ2000 in deg
            'majdiam': None, -> maj diameter
            'name': u'test1' -> name of the source
        }



        """
        # initiate prepared for photometry flag
        self.prepared = False

        # load observation setup
        self.obs = observation

        # set init imfile
        self.imfile = self.obs.files['imfile']
        # # export original image
        ct.exportfits(self.imfile, self.obs.files['fitsfile'])

    @property
    def prepared(self):
        return self.__prepared

    @prepared.setter
    def prepared(self, prepared):
        self.__prepared = prepared

    @property
    def imfile(self):
        return self.__imfile

    @imfile.setter
    def imfile(self, imfile):
        self.__imfile = imfile

    @property
    def fitsfile(self):
        return self.__fitsfile

    @fitsfile.setter
    def fitsfile(self, fitsfile):
        self.__fitsfile = fitsfile

    # getter and setter for blobcat parameters
    @property
    def blobpars(self):
        return self.__blobpars

    # getter and setter for background statistics
    @property
    def bckgstats(self):
        return self.__bckgstats

    def set_bckgstats(self, region, stats=None):
        # stats in the 'region_background'
        if stats is None:
            stats = run_imstat(in_image=self.imfile, in_region=region)
            stats['background'] = math.sqrt(stats['rms'] ** 2 - stats['sigma'] ** 2)
        self.__bckgstats = stats
        return self

    # getter for blob photometry
    @property
    def blobresults(self):
        return self.__blobresults

    @blobresults.setter
    def blobresults(self, results):
        self.__blobresults = results

    def aperture_phot(self, image, aperture):
        mask = "'{}'>{}".format(image, self.zerolevel)
        return run_imstat(image, aperture, mask)

    # gauss fitting results getter and setter
    @property
    def fitresults(self):
        return self.__fitresults

    @fitresults.setter
    def fitresults(self, results):
        self.__fitresults = results

    # gauss fitting estimates file getter and setter
    # see ESTIMATES in  https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imfit/about
    @property
    def fit_estimates_file(self):
        return self.__fit_estimates_file

    @fit_estimates_file.setter
    def fit_estimates_file(self, estimates_file):
        self.__fit_estimates_file = estimates_file

    def prepare_photometry(self):
        # copy original image to 'imcleaned' image and work on that one
        shutil.copytree(self.imfile, self.obs.files['imcleaned'])
        self.imfile = self.obs.files['imcleaned']

        # subtract background point sources if any
        subtract_bckgs(self.obs.pars['bckg_sources'], self.imfile, self.obs.pars['use_bmaj'])
        # region for the backgorund statistics in CASA format
        region_background = make_annulus(self.obs.target['draj2000'],self.obs.target['ddecj2000'],
                                         self.obs.pars['annulus_in'],self.obs.pars['annulus_out'])

        self.set_bckgstats(region_background)

        # make fits image from im
        ct.exportfits(self.imfile, self.obs.files['fitscleaned'])
        self.fitsfile = self.obs.files['fitscleaned']

        #TODO check if everything is ready
        self.prepared = True
        return self

    def out_results(self):

        set_hash = ibstrings.id_generator(32)

        res = ibdicts.merge_dicts([
            {'hash': set_hash},
            self.obs.target,
            self.obs.files,
            self.bckgstats,
            self.obs.pars,
            self.fitresults,
            self.blobresults,
            self.blobpars
            ])
        return res

    def gaussfit(self):

        if not self.prepared:
            self.prepare_photometry()

        # region for gaussian fitting in CASA format
        region_fit = 'circle[[{}deg,{}deg],{}arcsec]'.format(self.obs.target['draj2000'],
                                                             self.obs.target['ddecj2000'],
                                                             self.obs.pars['annulus_in'])

        # run source fitting
        fit_input = {
            'imagename': self.imfile,
            'region'   : region_fit,
            'logfile'  : self.obs.files['logtable'],
            'dooff'    : True,
            'offset'   : self.bckgstats['background'],
            'fixoffset': self.obs.pars['fix_offset'],
            'summary'  : self.obs.files['summaryfile'],
            'model'    : self.obs.files['modelfile'],
            'estimates': self.fit_estimates_file
            }

        fitresults = run_imfit(fit_input)

        if not fitresults:
            return False

        parsedresults = parse_fit(fitresults)

        # calculate offset from original position
        parsedresults['offset_fit'] = radec_separation(parsedresults['fit_posra'],
                                                       parsedresults['fit_posdec'],
                                                       self.obs.target['draj2000'],
                                                       self.obs.target['ddecj2000'])
        self.fitresults = parsedresults
        return self

    def gaussfit_fixed(self):
        # run source fitting with fixed estimates
        # estimates are made from temp gauss fitting run

        temp_parsed = self.gaussfit()
        if not temp_parsed:
            return False

        estimates = {
            'estimates_file': self.obs.files['imfitestims'],
            'peak'          : temp_parsed['peak'],
            'peak_x'        : temp_parsed['peak_x'],
            'peak_y'        : temp_parsed['peak_y'],
            'maj_axis'      : self.obs.pars['use_bmaj'],
            'min_axis'      : self.obs.pars['use_bmin'],
            'bpa'           : self.obs.pars['use_bpa'],
            'fixed'         : 'xyabp'
            }
        write_estimates(**estimates)
        self.fit_estimates_file = self.obs.files['imfitestims']

        self.gaussfit()

        return self

    # apply blobcat algorithm to find extended emission
    # see http://adsabs.harvard.edu/abs/2012MNRAS.425..979H
    # script source: http://blobcat.sourceforge.net/
    def blobit(self):

        if not self.prepared:
            self.prepare_photometry()

        self.__blobpars = {
            'rmsval': self.bckgstats['rms'],
            'bmaj'  : self.obs.pars['use_bmaj'],
            'bmin'  : self.obs.pars['use_bmin'],
            'bpa'   : self.obs.pars['use_bpa'],
            'dSNR'  : self.obs.pars['dSNR'],  # self.aprtrstats['aprtr_median'] / self.bckgstats['rms'],
            'fSNR'  : self.obs.pars['fSNR'],  # self.aprtrstats['aprtr_q1'] / self.bckgstats['rms'],
            # 'dSNR': self.aprtrstats['aprtr_median'] / self.bckgstats['rms'] + 1,
            # 'fSNR': self.aprtrstats['aprtr_q1'] / self.bckgstats['rms'] + 1,
            'hfill' : self.masklevel
            }
        if isinstance(self.obs.pars['blobpix_minmax'][0], int):
            self.__blobpars['maxpix'] = self.obs.pars['blobpix_minmax'][0],

        if isinstance(self.obs.pars['blobpix_minmax'][1], int):
            self.__blobpars['minpix'] = self.obs.pars['blobpix_minmax'][1],

        pars = ''
        for par in self.__blobpars:
            pars = "{} --{}={}".format(pars, par, self.__blobpars[par])

        tmpimage = "tmpblobs.fits"
        tmpblobs = "tmpblobs_blobs.fits"

        shutil.copy(self.fitsfile, tmpimage)

        command = "blobcat {} --write {}".format(tmpimage,pars)
        os.system(command)

        if not os.path.exists(tmpblobs):
            return False

        shutil.move(tmpblobs, self.obs.files['fitsfile_blobs'])
        ct.importfits(fitsimage=self.obs.files['fitsfile_blobs'], imagename=self.obs.files['imfile_blobs'])
        expression = 'iif(IM0<{},{},IM1)'.format(self.masklevel, self.zerolevel)
        ct.immath(imagename=[self.obs.files['imfile_blobs'], self.imfile], mode='evalexpr',
               outfile=self.obs.files['maskedblobs'], expr=expression)
        ct.exportfits(fitsimage=self.obs.files['maskedblobsfits'], imagename=self.obs.files['maskedblobs'])

        # region for apperture fitting in CASA format
        region_aperture = make_aperture(self.obs.target['draj2000'],self.obs.target['ddecj2000'],
                                        self.obs.pars['aperture'])

        tmp_blobresults = self.aperture_phot(self.obs.files['maskedblobs'], region_aperture)

        blobresults = {}
        # add blob prefix to all keys
        for blobkey in tmp_blobresults:
            blobresults["blob_{}".format(blobkey)] = tmp_blobresults[blobkey]

        # number of beams
        blobresults['blob_no_beams'] = abs(self.obs.pars['cdelt1'] * self.obs.pars['cdelt2']) * blobresults[
            'blob_npts'] / self.obs.pars['beam_area']

        # correct for background flux
        blobresults['blob_subtr_flux'] = blobresults['blob_flux'] - blobresults['blob_no_beams'] * self.bckgstats[
            'background']

        self.blobresults = blobresults
        return self
